import React, { Component, Fragment } from "react";
import { Container } from "reactstrap";
import Navigation from "../components/Navbar";

class Post extends Component {
  render() {
    const { title } = this.props;
    return <h5>{title}</h5>;
  }
}

class Home extends Component {
  state = {
    posts: [],
  };
  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((response) => response.json())
      .then((result) => {
        this.setState({ posts: result });
      });
  }
  render() {
    return (
      <Fragment>
        <Navigation />
        <Container className="p-4">
          {this.state.posts.map(i => <Post title={i.title} key={i.id} />)}
        </Container>
      </Fragment>
    );
  }
}

export default Home;
