import React, { Component } from "react";
// import logo from "./logo.svg";
import "./App.css";
// import Submit from "./Submit";
// import { Alert, Button, Container } from "reactstrap";
// import styles from "./style.module.css";
import {
  BrowserRouter as Router,
  Route,
  Routes as Switch,
} from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/About";
import Contact from "./pages/Contact";
import Login from "./pages/Login";

class App extends Component {
  state = { danger: false };

  handleOnClick = (event) => {
    this.setState({ danger: !this.state.danger });
  };

  // render() {
  //   const { danger } = this.state;
  //   return (
  //     <Container className={styles.container}>
  //       <Container className="text-center">
  //         <Button className="mb-4" color="danger" onClick={this.handleOnClick}>
  //           Hati-hati!
  //         </Button>
  //         {danger && <Alert color="danger">Sudah dibilang hati-hati..</Alert>}
  //       </Container>
  //     </Container>
  //   );
  // }
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/login" element={<Login />}></Route>
          <Route path="/about" element={<About />}></Route>
          <Route path="/contact" element={<Contact />}></Route>
          <Route path="/" element={<Home />}></Route>
        </Switch>
      </Router>
    );
  }
}

export default App;
